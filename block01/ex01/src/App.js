import React from "react"
import './App.css';

const App = () => {
    const names = ["t-shirts", "hats", "shorts", "jackets","pants","shoes"]

    const renderNames = (data) => {
        return data.map((name, i) => {
            return <h1 key={i}> Hello {name}</h1>
        })
    }
    return <div> {renderNames(names)} </div>
}

export default App;
