import logo from './logo.svg';
import React from 'react';
import './App.css';
import {v4 as uuid} from "uuid"

const App = () => {
    const products = [
        {
            product: "flash t-shirt",
            price: 27.5,
            category: "t-shirts",
            bestSeller: false,
            image:
                "https://images-na.ssl-images-amazon.com/images/I/61ZipyCaAKL._AC_UX385_.jpg",
            onSale: true,
        },
        {
            product: "batman t-shirt",
            price: 22.5,
            category: "t-shirts",
            bestSeller: true,
            image:
                "https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png",
            onSale: false,
        },
        {
            product: "superman hat",
            price: 13.9,
            category: "hats",
            bestSeller: true,
            image:
                "https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg",
            onSale: false,
        },
    ]
    const newProducts = products.map((product) => ({...product, id: uuid()}));

    const renderAllproducts = (data) => {
        return data.map((name) => {
            return (
                <ul key={name.id}>
                    <li>{name.product}</li>
                    <li>{name.price}</li>
                    <img src={name.image}/>
                    {name.onSale ? 'On Sale' : ''}
                </ul>

            )
        })
    }

    const renderBestSeller = (data) => {
        return data.map((name) => {
            if (name.bestSeller) {
                return (
                    <ul key={name.id}>
                        <li>{name.product}</li>
                        <li>{name.price}</li>
                        <img src={name.image}/>
                        {name.onSale ? 'On Sale' : ''}
                    </ul>

                )
            }
        })
    }
    return (
        <div>
            <header>My Shop</header>
            <h1>All Products</h1>
            {renderAllproducts(newProducts)}
            <h1>Best Sellers</h1>
            {renderBestSeller(newProducts)}
            <footer>All rights reserved</footer>
        </div>
    )

}

export default App;
