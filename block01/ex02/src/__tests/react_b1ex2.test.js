import "@testing-library/jest-dom";
import { screen, render } from "@testing-library/react";
import Test from "../App.js";

beforeEach(() => render(<Test />));

describe("b1ex02: Display all products and best-sellers with a header and a footer", () => {
  it("Must contain a header tag", () => {
    expect(screen.getByRole("banner")).toHaveTextContent("My Shop");
  });
  it("Must contain a footer tag", () => {
    expect(screen.getByRole("contentinfo")).toHaveTextContent(
      "All rights reserved"
    );
  });
  it("Must contain the batman shirt twice", () => {
    const batman_shirts = screen.getAllByText("batman t-shirt");
    expect(batman_shirts).toHaveLength(2);
  });
  it("Must contain the superman hat twice", () => {
    const superman_hat = screen.getAllByText("superman hat");
    expect(superman_hat).toHaveLength(2);
  });
  it("Must contain the flash t-shirt once", () => {
    const flash_shirt = screen.getAllByText("flash t-shirt");
    expect(flash_shirt).toHaveLength(1);
  });
  it("Must contain the batman shirt price twice", () => {
    const batman_shirts = screen.getAllByText("22.5");
    expect(batman_shirts).toHaveLength(2);
  });
  it("Must contain the superman hat price twice", () => {
    const superman_hat = screen.getAllByText("13.9");
    expect(superman_hat).toHaveLength(2);
  });
  it("Must contain the flash t-shirt price once", () => {
    const flash_shirt = screen.getAllByText("27.5");
    expect(flash_shirt).toHaveLength(1);
  });
  it("Must contain the flash t-shirt image once", () => {
    const images = screen
      .getAllByRole("img")
      .filter(
        (image) =>
          image.src ===
          "https://images-na.ssl-images-amazon.com/images/I/61ZipyCaAKL._AC_UX385_.jpg"
      );
    expect(images).toHaveLength(1);
  });
  it("Must contain the batman shirt image twice", () => {
    const images = screen
      .getAllByRole("img")
      .filter(
        (image) =>
          image.src ===
          "https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png"
      );
    expect(images).toHaveLength(2);
  });
  it("Must contain the superman hat image twice", () => {
    const images = screen
      .getAllByRole("img")
      .filter(
        (image) =>
          image.src ===
          "https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg"
      );
    expect(images).toHaveLength(2);
  });
  it("Must contain two h1", () => {
    screen.getAllByRole("heading", { level: 1 }).forEach((heading) => {
      expect(heading).toBeInTheDocument();
    });
  });
});
