import React from 'react';
import './App.css';
import Child from './Child';

class App extends React.Component{

    render() {
        const myName = 'Mehul';
        return (
            <div className="App">
                <Child name={myName}/>
            </div>
        )
    }
}

export default App;