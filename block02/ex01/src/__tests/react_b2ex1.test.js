import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "@testing-library/jest-dom";
import Test from "../App.js";
import Child from "../Child.js";

configure({ adapter: new Adapter() });

const parent = shallow(<Test />);
const children = parent.props().children;
const noPropsChild = shallow(<Child />);

describe("b2ex01: Pass some prop from class parent component to class child component", () => {
  it("Name must be a class component", () => {
    let isThereClassComponent = false;

    children.forEach((child, i) => {
      if (`${child.type}`.includes("Component")) isThereClassComponent = true;
    });

    if (!isThereClassComponent) return false;
  });

  it("Must have a prop rendered in child component", async () => {
    let isThereAnUndefined = false;

    if (
      noPropsChild.props().children.props?.children &&
      noPropsChild.props().children.props.children.includes(undefined)
    ) {
      isThereAnUndefined = true;
    } else if (noPropsChild.props().children.includes(undefined)) {
      isThereAnUndefined = true;
    }

    if (!isThereAnUndefined) return false;
  });

  it("There must be a match between passed prop and child rendering", async () => {
    let match = false;

    let classComponent = children.filter((child) =>
      `${child.type}`.includes("Component")
    )[0];
    let props = Object.entries(classComponent.props);

    props.forEach((prop) => {
      const childWithProps = shallow(<Child name={`${prop[1]}`} />);
      let childCheck = childWithProps.props().children.props?.children
        ? childWithProps.props().children.props.children
        : childWithProps.props().children;

      childCheck.forEach((child) => {
        prop === child && (match = true);
      });
    });

    if (!match) return false;
  });
});
