import React from 'react';
import './App.css';
import Name from './Name';
import Surname from "./Surname";

class App extends React.Component{

  render() {
      let names = ["Clifford", "Russel", "Michael", "Dennis", "Andre"];
      let surnames = ["Smith", "Simmons", "Diamond", "Coles", "Benjamin"];

      return(
          <div className="App">
              {names.map((name,idx) => (
                  <div key={idx}>
                  <Name name={name}/>
                  <Surname surname={surnames[idx]}/>
                  </div>
                  ))}
          </div>
      );
  }

}

export default App;