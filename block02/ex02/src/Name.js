import React from 'react'

class Name extends React.Component{

    render(){
        return (
            <>
                {this.props.name}
            </>
        )
    }
}

export default Name;