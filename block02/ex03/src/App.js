import React from 'react';
import './App.css';
import List from "./List";

class App extends React.Component{

  render() {
    const categories = ["shirts", "hats", "shoes"];

    return(
        <div className="App">
                <List categories={categories}/>
        </div>
    );
  }

}

export default App;