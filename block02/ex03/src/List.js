import React from 'react'
import Item from "./Item";

class List extends React.Component{

    render(){
        return (
             <>
                {this.props.categories?.map((name,idx) => (
                        <Item category={name}  key={idx}/>

                ))}
             </>
        )
    }
}

export default List;