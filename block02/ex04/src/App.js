import React from 'react';
import './App.css';
import List from "./List";

const App = () => {
  const categories = ["shirts", "hats", "shoes"];

  return (
      <div className="App">
        <List categories={categories}/>
      </div>);
}

export default App;