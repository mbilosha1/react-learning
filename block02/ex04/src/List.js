import React from 'react'
import Item from "./Item";

const List = (props) => {

    let { categories } = props
    return (
        <>
            {categories?.map((category,idx) => (
                <div key={idx}>
                    <Item category={category}/>
                </div>
            ))}
        </>
    );
}

export default List;

List.defaultProps = {
    names : ['shirts','hats','shoes']
}