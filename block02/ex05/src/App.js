import React from 'react';
import './App.css';
import List from './List'


const App = () => {
  const productsList = [
    {
      product: "flash t-shirt",
      price: 27.5,
      category: "t-shirts",
      bestSeller: false,
      image:
          "https://images-na.ssl-images-amazon.com/images/I/61ZipyCaAKL._AC_UX385_.jpg",
      onSale: true,
    },
    {
      product: "batman t-shirt",
      price: 22.5,
      category: "t-shirts",
      bestSeller: true,
      image:
          "https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png",
      onSale: false,
    },
    {
      product: "superman hat",
      price: 13.9,
      category: "hats",
      bestSeller: true,
      image:
          "https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg",
      onSale: false,
    },
  ]

  const bestSellerproductsList = productsList.filter(function (e) {
    return e.bestSeller = true;
  });

  return (
      <div className="wrapper">
        <div className="header clearfix">
            <div className="f1 logo">
                <a href="/"><img src="https://cdn.shopify.com/s/files/1/0074/8332/t/3/assets/logo.png?v=9670308648985274274" alt="My Shop"/></a>
            </div>
        </div>
        <div className="body clearfix">
          <div id="sub_navigation">
            <h6>All Products</h6>
          </div>
          <div id="collection">
            <div className="outer">
              <div id="products" className="products clearfix">
                <div className="row clearfix">
                  <List products={productsList}/>
                </div>
              </div>
            </div>
            <div className="outer-featured">
              <div id="products" className="products clearfix">
                <div className="row clearfix">
                  <List products={bestSellerproductsList} />
                </div>
              </div>
            </div>
          </div>
          </div>
          <div className="footer clearfix">
            <div className="block">
              <footer>All rights reserved</footer>
            </div>

          </div>
        </div>
  );

}

export default App;