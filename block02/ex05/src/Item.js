import React from 'react'
import './Item.css';

const Item = (props) => {
    let {item} = props;

    return (
        <div className="product">
            <div className="image">
                <img src={item.image} alt=""/>
            </div>
            <div className="details">
                {item.product}
            </div>
            <span className="price">
                    {item.price}
            </span>
            {item.onSale ? 'On Sale' : ''}
        </div>
    )
}

export default Item;

Item.defaultProps = {
    item : []
}