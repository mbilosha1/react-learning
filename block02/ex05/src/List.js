import React from 'react'
import Item from "./Item";
import {v4 as uuid} from "uuid"

const List = (props) => {

    let { products } = props
    const newProducts = products.map((product) => ({...product, id: uuid()}));

    return (
        <>
            {newProducts.map((product) => (
                <ul key={product.id}>
                    <Item item={product}/>
                </ul>
            ))}
        </>
    );
}

export default List;

List.defaultProps = {
  products : []
}