import React, { Component } from 'react';

class App extends Component {
  state = {
    task :'no data provided!'
  }

  handleChange = event => {
    let name = event.target.name;
    let value = event.target.value;
    if(value === ''){
        value = 'no data provided!'
    }
    this.setState({[name]:value});
  }

  render() {
    return (
        <div>
            <input
                onChange = {this.handleChange}
                name= 'task'
            />

          <h1>{this.state.task}</h1>
        </div>
    );
  }
}

export default App;