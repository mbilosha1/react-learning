import React, { useState } from 'react';

const App = () => {

  const [task, setTask] = useState('no data provided!');

  const handleTaskChange = (e) => {
      let value = e.target.value === '' ? 'no data provided!' : e.target.value;
      setTask(value);
  }

  return (
      <div className='main'>

          <div className='child'>Task
            <input onChange={handleTaskChange}/>

          </div>

        <h1>{task}</h1>
      </div>
  );
}

export default  App