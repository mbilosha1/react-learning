import React, { useState } from 'react';
import User from "./User";
import Email from "./Email";
import Submit from "./Submit";

const App = () => {

    const [user, setUser] = useState('');
    const [email, setEmail] = useState('');


    return (
        <div className='main'>
            <h1>{user}</h1>
            <h1>{email}</h1>
            <form>
                <User
                    changeUser={user => setUser(user)}/>
                <Email
                    changeEmail={email => setEmail(email)}/>
            </form>
            <Submit
                changeText={() => alert(email + '' + user)}/>
        </div>
    );
}

export default  App