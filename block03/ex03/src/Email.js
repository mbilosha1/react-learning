import React from 'react';

const Email = (props) => {

    const handleData = (e) => {
        let value = e.target.value === '' ? '' : e.target.value;
        props.changeEmail(value);
    }

    return (
        <>
            <input
                    onChange = {handleData}
                    name= 'email'
                />
        </>
    );
}

export default  Email