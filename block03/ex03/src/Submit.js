import React from 'react';

const Submit = (props) => {

    const handleClick = () => {
         props.changeText();
    }

    return (
        <>
            <div>
                <button onClick={handleClick}>Click me</button>
            </div>
        </>
    );
}

export default  Submit