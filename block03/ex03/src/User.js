import React from 'react';

const User = (props) => {

    const handleData = (e) => {
        let value = e.target.value === '' ? '' : e.target.value;
        props.changeUser(value);
    }

    return (
        <>
                <input
                    onChange = {handleData}
                    name= 'user'
                />
        </>
    );
}

export default  User