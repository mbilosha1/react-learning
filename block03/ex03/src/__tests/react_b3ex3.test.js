import { configure, mount, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "@testing-library/jest-dom";
import App from "../App.js";

configure({ adapter: new Adapter() });

const app = mount(<App />);
const children = shallow(<App />).props().children;

describe("b3ex03: Form handling", () => {
  it("There should be three functional components in App.js", () => {
    let appHolds3FunctionalComps = true;
    children.forEach((child) => {
      if (!`${child.type}`.includes("function")) {
        appHolds3FunctionalComps = false;
      }
    });

    if (!appHolds3FunctionalComps) return false;
  });
  it("There should be two inputs with onChange event in App.js", () => {
    let areThereInputsWithOnChangeEvent = true;
    const inputs = app.find("input");
    inputs.forEach((input, idx) => {
      if (input.prop("onChange") === undefined) {
        areThereInputsWithOnChangeEvent = false;
      }
    });
    if (!areThereInputsWithOnChangeEvent) return false;
  });
  it("There should be a button with onClick event in App.js", () => {
    let areThereInputsWithOnChangeEvent = false;
    const button = app.find("button");
    if (button.prop("onClick") !== undefined) {
      areThereInputsWithOnChangeEvent = true;
    }
    if (!areThereInputsWithOnChangeEvent) return false;
  });
  it("Alert should show user's input", () => {
    let text;
    const inputs = app.find("input");
    inputs.forEach((input, idx) =>
      idx === 0
        ? input.simulate("change", { target: { value: "Hello" } })
        : idx === 1 && input.simulate("change", { target: { value: "World" } })
    );
    window.alert = (e) => (text = e);
    const button = app.find("button");
    button.simulate("click");
    expect(text).toContain("Hello");
    expect(text).toContain("World");
  });
});
