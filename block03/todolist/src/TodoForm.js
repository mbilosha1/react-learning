import React, { useState } from "react";
import { v4 as uuid } from "uuid";
// import './TodoForm.css';

function TodoForm(props) {
  const [todos, setTodos] = useState([]);
  const [tempTodo, setTempTodo] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    if (tempTodo === "") return false;
    let value = [...todos];
    value.push({ id: uuid(), text: tempTodo, isComplete: false });
    setTodos(value);
    setTempTodo("");
  };

  const handleTodoChange = (e) => {
    let value = e.target.value;
    setTempTodo(value);
  };

  const handleDelete = (e) => {
    const val = todos.filter((item) => item.id !== e.target.value);
    setTodos(val);
  };

  const handleComplete = (e) => {
    let index = todos.findIndex((x) => x.id === e.target.value);
    let tempTodos = [...todos];
    tempTodos[index].isComplete = !tempTodos[index].isComplete;
    setTodos(tempTodos);
  };

  return (
    <div className="todo-input">
      <h1>Please add your todo list items here:</h1>
      <form onSubmit={handleSubmit}>
        <input type="text" name="todo" onChange={handleTodoChange} value={tempTodo}/>
        <div className="todo-submit">
          <button>Add Todo Item</button>
        </div>
      </form>
      <div className="todo-list">
        <h1>Todo Items</h1>
        {todos
          .filter((todo) => todo.isComplete === false)
          .map((ele) => (
            <div className="card" key={ele.id}>
              <span>
                {ele.text}&nbsp;
                <button  value={ele.id} onClick={handleDelete}>
                  Delete Todo
                </button>&nbsp;
                <button value={ele.id} onClick={handleComplete}>
                  Mark Complete
                </button>
              </span>
            </div>
          ))}
      </div>
      <div>
        <h1>Todo Completed Items</h1>
        {todos
          .filter((todo) => todo.isComplete === true)
          .map((ele) => (
            <div key={ele.id}>
              <span>{ele.text}</span>
            </div>
          ))}
      </div>
    </div>
  );
}

export default TodoForm;