import React, { useState, useEffect } from 'react';

function App() {
  const [counter, setCounter] = useState(0);
  const [display, setDisplay] = useState(true);

 const handleClick = () => {
      setCounter(counter + 1);
  }

  useEffect( () => {
      const showCount = () => {
          if (counter === 0) return;
          counter % 2 === 1 ?  setDisplay(true) :  setDisplay(false);
      };
        showCount();
  }, [counter]);

 const func = () => {
         if (!display) {
             return  <h2>{counter - 1}</h2>;
         }
            return <h2>{counter}</h2>
 }

  return (
      <div>
        <button onClick={handleClick}>counter</button>
          {func()}
      </div>
  )
}

export default App;