import React, { useState, useEffect } from 'react';
import './App.css';
import Input from "./Input";
import Rate from "./Rate";
import axios from "axios";

function App() {

    const [input, setInput] = useState(0);
    const [rate, setRate] = useState(0);
    const [perRate, setPerRate] = useState(1.17);

    useEffect( () => {

        const apirate = async () => {
            let tempRate = '';
            let url = `http://apilayer.net/api/live?access_key=6cade77a0a6505d1d562fb3e944555b4&currencies=EUR&source=USD&format=1`;
            await axios.get(url)
                .then((res)=>{
                    tempRate = res.data['quotes']['USDEUR'];
                    setPerRate(1/tempRate);
                })
                .catch((error)=>{
                    console.log('error:', error);
                })
        }
        apirate();
        const getRate = () => {
            let updated = input * perRate;
            setRate(updated.toFixed(2));
        };
        getRate();
    }, [input]);

  return (
    <div className="App">
      <Input changeInput={input => setInput(input)} />
      <Rate rate={rate} input={input} />
    </div>
  );
}

export default App;