import React from 'react';

function Input(props) {
    const handleChange = (e) => {
        let value = e.target.value;
        props.changeInput(value);
    }

    return (
        <div>
            <form>
                <label>Enter here to convert EUR to USD</label>
                <br/>
                <input onChange={handleChange} type="number" name="number" />
            </form>
        </div>
    );
}

export default Input;