import React from 'react';

function Rate(props) {

    return (
        <h1>
            {props.input} EUR converts to {props.rate} USD
        </h1>
    );
}

export default Rate;