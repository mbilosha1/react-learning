import { configure, mount, shallow } from "enzyme";
import "@testing-library/jest-dom";
import App from "../App.js";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

configure({ adapter: new Adapter() });

const app = mount(<App />);
const children = shallow(<App />).props().children;

describe("b4ex02: Converter EUR -> USD", () => {
  it("Has 2 functional components", () => {
    let appHolds3FunctionalComps = true;
    children.forEach((child) => {
      if (!`${child.type}`.includes("function")) {
        appHolds3FunctionalComps = false;
      }
    });

    if (!appHolds3FunctionalComps) return false;
  });
  it("Has input type number with onChange event", () => {
    let isThereInputWithOnChangeEvent = false;
    const input = app.find("input");
    if (
      input.prop("onChange") !== undefined &&
      input.props().type === "number"
    ) {
      isThereInputWithOnChangeEvent = true;
    }

    if (!isThereInputWithOnChangeEvent) return false;
  });
  it("Has an h1 initialised to 0 and rendering conversion", async () => {
    let h1WorkingFine = false;

    const h1 = app.find("h1");
    if (parseInt(h1.text()) === 0) h1WorkingFine = true;

    const input = app.find("input");
    input.simulate("change", { target: { value: "10" } });

    const h1_ = app.find("h1");
    const rate = await getData();

    if (
      Number(h1_.text()) === 11.7 ||
      Number(h1_.text()) === 10 / rate.quotes.USDEUR
    )
      h1WorkingFine = true;

    if (!h1WorkingFine) return false;
  });
});

const getData = async () => {
  try {
    const data = await fetch(
      "http://api.currencylayer.com/live?access_key=6741ed392c427ff3c0d5c35dee08fcf3&format=1"
    )
      .then((res) => res.json())
      .then((res) => res);
    return data;
  } catch (err) {
    console.error(err);
  }
};
