import React,{useEffect, useState} from "react";
import './App.css';

function App() {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [match, setMatch] = useState('');
    const [message,setMessage] = useState('');

    const users = [
        { email: "a@a.com", password: "a" },
        { email: "b@b.com", password: "b" },
        { email: "c@c.com", password: "c" },
    ];

    const handleSubmit = (e) => {
        e.preventDefault();
        const found = users.find(user => (user.email === email && user.password === password));
        if (found == undefined){
            setMatch(false);
        }
        else{
            setMatch(true);
        }
    }

    useEffect( () => {

    }, [match,message]);

    const handleData = (e) => {
        let value = e.target.value;
        (e.target.name === 'email') ? setEmail(value) : setPassword(value);
    }

    const func = () => {
        let seconds = 3;
        if (match === true){
            console.log(`This message will disappear in ${seconds}`);
            seconds--;
            const  interval = setInterval(() => {
                if(seconds > 0){
                    console.log(`This message will disappear in ${seconds}`);
                    seconds--;
                }
                else{
                    clearInterval(interval);
                }
            },1000);
            return <h2>Match found successfully</h2>;
        } else if (match === false){
            console.log(`This message will disappear in ${seconds}`);
            seconds--;
            const  interval = setInterval(() => {
                if(seconds > 0){
                    console.log(`This message will disappear in ${seconds}`);
                    seconds--;
                }
                else{
                    clearInterval(interval);
                }
            },1000);

        }
    }

  return (
    <div className="App">
      <form onSubmit={handleSubmit}>
          <br/>
        Email : &nbsp;<input name="email" type="email" onChange={handleData} value={email}/><br/><br/>
        Password : &nbsp;<input name="password" type="password" onChange={handleData} value={password}/><br/><br/>
          <button onClick={handleSubmit}>Submit</button>
      </form>
        {func()}
        {message}
    </div>
  );
}

export default App;