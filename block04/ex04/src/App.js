import React, { useState,useEffect }  from "react";
import './App.css';
import {v4 as uuid} from "uuid";

function App() {
  const [cartTotal, setCartTotal] = useState('0');
  // const [cartProducts, setCartProducts] = useState([]);
  const products = [
    {
      product: "flash t-shirt",
      price: 27.5,
      category: "t-shirts",
      bestSeller: false,
      image:
          "https://images-na.ssl-images-amazon.com/images/I/61ZipyCaAKL._AC_UX385_.jpg",
      onSale: true,
      quantity: 1,
    },
    {
      product: "batman t-shirt",
      price: 22.5,
      category: "t-shirts",
      bestSeller: true,
      image:
          "https://s1.thcdn.com/productimg/1600/1600/11676326-1444552242012324.png",
      onSale: false,
      quantity: 1,
    },
    {
      product: "superman hat",
      price: 13.9,
      category: "hats",
      bestSeller: true,
      image:
          "https://banner2.kisspng.com/20180429/rqe/kisspng-baseball-cap-superman-logo-batman-hat-5ae5ef317f8366.9727520615250184175223.jpg",
      onSale: false,
      quantity: 1
    },
  ];
  const newProducts = products.map((product) => ({...product, id: uuid()}));


  const handleDecrement = (e,i) => {
    console.log(i);
  };

  const handleIncrement = (e,i) => {
    console.log(i);
  };

  const getCartTotal = () => {
    let total = 0;
    newProducts.forEach(function (product) {
      total += product.quantity * product.price;
    });
    setCartTotal(total);
  }

  useEffect( () => {
    getCartTotal();
  }, [cartTotal]);

  return (
    <div className="App">
      {newProducts.map((product,id) => (
          <ul key={product.id}>
            <li>{product.product}</li>
            <span>
              <button type="button" onClick={(e,i) => {handleDecrement(e,id)}}>-</button>
              {product.quantity}
              <button type="button" onClick={(e,i) => {handleIncrement(e,id)} }>+</button>
            </span>
          </ul>
      ))}
      <h1>Total:{cartTotal}</h1>
    </div>
  );
}

export default App;
