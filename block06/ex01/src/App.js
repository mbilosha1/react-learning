import React from "react";
import About from "./About.js";
import User  from "./User.js";
import Contact  from "./Contact.js";
import Home  from "./Home.js";

import {BrowserRouter as Router, Navigate, Redirect, Route, Routes} from "react-router-dom";
import Navbar from "./Navbar";

function App() {

  return (
      <Router>
          <Navbar />
          <Routes>
              <Route exact path="/home"    element={<Home/>} />
              <Route path="/" element={<Navigate to="/home" />} />
              <Route exact path="/about"  element={<About/>} />
              <Route exact path="/contact" element={<Contact/>} />
              <Route exact path="/user" element={<User/>} />
          </Routes>
      </Router>
  )
}
export default App