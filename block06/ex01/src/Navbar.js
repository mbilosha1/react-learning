import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
    return (
        <div style={styles.navStyles}>
            <NavLink
                exact
                style={styles.default}
                activeStyle={styles.active}
                to={"/"}
            >Home
            </NavLink>

            <NavLink
                exact

                style={styles.default}

                activeStyle={styles.active}
                to={"/about"}
            >
                About
            </NavLink>
            <NavLink
                exact

                style={styles.default}

                activeStyle={styles.active}
                to={"/contact"}
            >
                Contact
            </NavLink>
            <NavLink
                exact

                style={styles.default}

                activeStyle={styles.active}
                to={"/user"}
            >
                User
            </NavLink>
        </div>
    );
}

export default Navbar;

const styles = {
    navStyles: {
        display: "flex",
        height: 60,
        alignItems: "center",
        justifyContent: "space-around"
    },
    active: {
        color: "red"
    },
    default: {
        textDecoration: "none",
        color: "black"
    }
};