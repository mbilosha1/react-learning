import React, { useState } from "react";
import MessageGeneric from "./MessageGeneric";

export default function App() {
  const [user] = useState({
    email: "mario@gmail.com",
    password: "12345678",
    cpassword: "12345678"
  });

  const [message, setMessage] = useState({});

  const [form, setValues] = useState({});

  const handleChange = (e) => {
    setValues({...form, [e.target.name]: e.target.value});
  }

  const handleSubmit = (e) => {
    e.preventDefault();
    if(!form.email || !form.email.match(/@/g) || form.email.match(/@/g).length !== 1){
      setMessage({
        header: "",
        body: "Please, provide a valid email",
        color: "red"
      });
      reInitializeMessage();
    }
    else if(!form.password || !form.cpassword || form.password.length < 8 ){
      setMessage({
        header: "",
        body: "Password must have at least 8 characters",
        color: "red"
      });
      reInitializeMessage();
    }
    else if(form.password !== form.cpassword ){
      setMessage({
        header: "",
        body: "Password should match",
        color: "red"
      });
      reInitializeMessage();
    }
    else if (form.email !== user.email || form.password !== user.password) {
      setMessage({
        header: "",
        body: "Somenthing went wrong",
        color: "#f2738c"
      });
      reInitializeMessage();
    } else {
      setMessage({
        header: "",
        body: "Successfully registered",
        color: "green"
      });
      reInitializeMessage();
    }
  };

  // clean up function
  const reInitializeMessage = () => {
    setTimeout(() => {
      setMessage({
        header: "",
        body: "",
        color: ""
      });
    }, [2000]);
  };

  return (
      <div>
        <MessageGeneric
            header={message.header}
            message={message.body}
            color={message.color}
        />
        <form style={styles.form} onChange={handleChange} onSubmit={handleSubmit}>
          <input name={"email"} placeholder="email"/>
          <input name={"password"} type="password" placeholder="password" />
          <input name={"cpassword"} type="password" placeholder="confirm password" />
          <button>click me</button>
        </form>
      </div>
  );
}
const styles = {
  form: {
    maxWidth: "630px",
    margin: "0 auto",
    display: "flex",
    flezDirection: "column",
    alignItems: "center",
    border: "1px solid gray",
    height: "200px",
    justifyContent: "center"
  }
};