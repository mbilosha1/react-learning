import React from "react";

class MessageGeneric extends React.Component {
    render() {
        let visible = {
            background: this.props.color,
            maxWidth: "420px",
            margin: "0 auto",
            padding: ".5rem",
            height: "200px",
            color: "white",
            display: "flex",
            alignItems: "center",
            flexDirection: "column"
        };

        return (

            <div

            >
                {/*<div style={{ marginTop: "5%", fontSize: 30 }}>{this.props.header}</div>*/}
                {/*<div style={{ marginTop: "10%", fontSize: 24 }}>*/}
                    <h2 style={{color: this.props.color}}>{this.props.message}</h2>
                {/*</div>*/}
            </div>
        );
    }
}

export default MessageGeneric;
