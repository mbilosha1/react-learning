import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "@testing-library/jest-dom";
import App from "../App.js";
import List from "../List.js";

configure({ adapter: new Adapter() });

const children = shallow(<App />).props().children;
const list = shallow(<List categories={["shoes", "hats", "shirts"]} />);
const listWithoutProps = shallow(<List />);

describe("b2ex03: Display categories ( Class )", () => {
  it("App.js must render class component List", () => {
    let listIsClassComponent = false;

    if (
      `${children?.type}`.includes("Component") ||
      `${children[0].type}`.includes("Component")
    )
      listIsClassComponent = true;

    if (!listIsClassComponent) return false;
  });
  it("List must render class component Item", () => {
    let itemIsClassComponent = false;

    list.props().children.forEach((child) => {
      if (`${child.type}`.includes("Component")) itemIsClassComponent = true;
    });

    if (!itemIsClassComponent) return false;
  });
  it("Pass categories array from App.js to List.js", () => {
    let categoriesPassedAsProps = false;

    if (children.props.categories.length === 3) categoriesPassedAsProps = true;
    if (
      children[0] &&
      children[0].props.categories.length > 0 &&
      listWithoutProps.props().children === undefined
    )
      categoriesPassedAsProps = true;

    if (!categoriesPassedAsProps) return false;
  });
  it("Pass category item from List.js to Item.js", () => {
    let categoryPassedAsProps = false;

    list.props().children.forEach((child) => {
      if (Object.keys(child.props)[0] === "category")
        categoryPassedAsProps = true;
    });

    if (!categoryPassedAsProps) return false;
  });
});
