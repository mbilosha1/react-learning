import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "@testing-library/jest-dom";
import App from "../App.js";
import List from "../List.js";

configure({ adapter: new Adapter() });

const children = shallow(<App />).props().children;
const list = shallow(<List categories={["shoes", "hats", "shirts"]} />);
const listWithoutProps = shallow(<List />);

describe("b2ex04: Display categories ( Functional )", () => {
  it("App.js must render functional component List", () => {
    let listIsFunctionalComponent = false;

    if (`${children.type}`.includes("function"))
      listIsFunctionalComponent = true;

    if (!listIsFunctionalComponent) return false;
  });
  it("List must render functional component Item", () => {
    let itemIsFunctionalComponent = false;

    list.props().children.forEach((child) => {
      if (`${child.type}`.includes("function"))
        itemIsFunctionalComponent = true;
    });

    if (!itemIsFunctionalComponent) return false;
  });
  it("Pass categories array from App.js to List.js", () => {
    let categoriesPassedAsProps = false;

    if (
      children.props.categories.length > 0 &&
      listWithoutProps.props().children === undefined
    )
      categoriesPassedAsProps = true;

    if (!categoriesPassedAsProps) return false;
  });
  it("Pass category item from List.js to Item.js", () => {
    let categoryPassedAsProps = false;

    list.props().children.forEach((child) => {
      if (Object.keys(child.props)[0] === "category")
        categoryPassedAsProps = true;
    });

    if (!categoryPassedAsProps) return false;
  });
});
