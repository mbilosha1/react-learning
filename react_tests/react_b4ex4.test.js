import { configure, mount } from "enzyme";
import "@testing-library/jest-dom";
import App from "../App.js";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";

configure({ adapter: new Adapter() });

const app = mount(<App />);

describe("b4ex04: Display cart", () => {
  it("App.js has 6 buttons with onClick event, two for each product", () => {
    let thereAre6Buttons = false;

    const buttons = app.find("button");
    buttons.forEach((btn) => {
      btn.prop("onClick") !== undefined
        ? (thereAre6Buttons = true)
        : (thereAre6Buttons = false);
    });

    if (!thereAre6Buttons || buttons.length !== 6) return false;
  });
  it("If total < 500 you should render 'Shipping will be charged' in red", () => {
    const total = app.find("h2");
    if (total.text().replace(/[^\d.-]/g, "") < 500) {
      const message = app.find({ children: "Shipping will be charged" });
      if (message.props().style.color !== "red") {
        return false;
      }
    } else return false;
  });

  it("Adding a product should change the total", () => {
    const total1 = app
      .find("h2")
      .text()
      .replace(/[^\d.-]/g, "");

    const found = app.find({ children: "+" });
    found.forEach((item, idx) => {
      idx === 0 && item.simulate("click");
    });

    const total2 = app
      .find("h2")
      .text()
      .replace(/[^\d.-]/g, "");

    if (total1 === total2) return false;
  });

  it("Pressing each button '+' 7 times will render green message 'Free Shipping'", () => {
    const found = app.find({ children: "+" });
    found.forEach((item) => {
      for (let i = 0; i < 7; i++) {
        item.simulate("click");
      }
    });

    const total = app.find("h2");
    if (total.text().replace(/[^\d.-]/g, "") >= 500) {
      const message = app.find({ children: "Free Shipping" });
      if (message.props().style.color !== "green") {
        return false;
      }
    } else return false;
  });
});
