import { configure, mount } from "enzyme";
import "@testing-library/jest-dom";
import App from "../App.js";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { act } from "react-dom/test-utils";

configure({ adapter: new Adapter() });

const app = mount(<App />);

describe("b6ex01: Initiation to Navigation", () => {
  it("In a functional App.js there should be 4 NavLinks", () => {
    const links = app.find("a");
    if (links.length !== 4) return false;
  });
  it("When clicking on About the URL should change to '/about'", () => {
    let testFail = false;
    const links = app.find("a");
    let href;

    act(() => {
      links.forEach((link) => {
        if (link.props().href !== "/") {
          link
            .getDOMNode()
            .dispatchEvent(new MouseEvent("click", { bubbles: true }));
          href = link.props().href;

          let finalURL = global.window.location.pathname;
          if (finalURL !== href) testFail = true;

          const app2 = mount(<App />);
          if (!app2.find("h1").text().toLowerCase().includes(href.slice(1)))
            testFail = true;
        }
      });
    });

    if (testFail) return false;
  });
});
