import Enzyme, { configure, shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import "@testing-library/jest-dom";
import App from "../../block03/ex02/src/App.js";

Enzyme.configure({ adapter: new Adapter() });


const app = shallow(<App />);

describe("b2ex05: Display categories", () => {
  it("There should be an input in App.js with an onChange event", () => {
    let isThereInputWithOnChangeEvent = false;
    const input = app.find("input");
    if (input.prop("onChange") !== undefined) {
      isThereInputWithOnChangeEvent = true;
    }
    if (!isThereInputWithOnChangeEvent) return false;
  });
  // it("There should be an h1 component initialised to 'no data provided!'", () => {
  //   let h1InitialisedCorrectly = false;

  //   const h1 = app.find("h1");
  //   if (h1.html().includes("data") === true) h1InitialisedCorrectly = true;

  //   if (!h1InitialisedCorrectly) return false;
  // });
  // it("There should be an h1 component rendering state", () => {
  //   let h1RenderingState = false;

  //   const state = Object.keys(app.state())[0];
  //   app.setState({ [state]: "Test" });
  //   const h1 = app.find("h1");
  //   if (h1.html().includes("data") === false) h1RenderingState = true;

  //   if (!h1RenderingState) return false;
  // });
});
