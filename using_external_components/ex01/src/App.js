import React, { useState } from 'react'
import { Button, Confirm } from 'semantic-ui-react'

function  App() {
    const [state, setState] = useState({ open: false, result: 'show the modal to capture a result' });

    const show = () => setState({ open: true })
    const handleConfirm = () => setState({ result: 'confirmed', open: false })
    const handleCancel = () => setState({ result: 'cancelled', open: false })

    return (
        <div>
            <p>
                Result: <em>{state.result}</em>
            </p>

            <Button onClick={show}>Show</Button>
            <Confirm
                open={state.open}
                onCancel={handleCancel}
                onConfirm={handleConfirm}
            />
        </div>
    )

}

export default App