import React, { useState, useEffect } from "react";
import { Button, Modal } from "react-bootstrap";

function App() {
  // const [show, setShow] = useState(false);
  // const [result, setResult] = useState("");
  const [seconds, setSeconds] = useState(null);

  // const handleClose = (e) => {
  //   setShow(false);
  //   if (e.target.value === "confirm") {
  //     setResult("Confirmed");
  //   } else if (e.target.value === "close") {
  //     setResult("Canceled");
  //   }
  // };

  // const handleShow = () => {
  //   setShow(true);
  // };

  useEffect(() => {
    intervalFunc();
  }, []);

  const intervalFunc = () => {
    let realSecs = 3;

    let interval = setInterval(() => {
      if (realSecs > 0) {
        setSeconds("banana" + realSecs);
        realSecs -= 1;
      } else {
        setSeconds(null);
        clearInterval(interval);
      }
    }, 1000);
  };

  return (
    <div className="App">
      {/* <Button variant="primary" onClick={handleShow}>
        Launch demo modal
      </Button> */}
      <Button onClick={() => intervalFunc()}>Click me!</Button>
      <p>{seconds}</p>
      {/* <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" value="close" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" value="confirm" onClick={handleClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal> */}
    </div>
  );
}

export default App;
